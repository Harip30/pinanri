package basyafinance.accountsaving.controller;

import basyafinance.accountsaving.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
@RequestMapping("/api/registrasi")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping("/step1/create")
    public ResponseEntity<String> simpanCustomer(@RequestParam("noTelepon") String noTelepon) {
        //validate angka no tpon
        Boolean numeric = isNumeric(noTelepon);
        System.out.println("proses create data");
        log.info("proses create data");

        if (numeric) {
            //servive saving
            ResponseEntity<String> stringResponseEntity = customerService.customerSave(noTelepon);
            return stringResponseEntity;

        } else {
            return ResponseEntity.ok("code:99" + "/n" + "desc:no telpn tidak valid");
        }


    }

    @PostMapping("step2/registration")
    public ResponseEntity<String> customerKomplit(@RequestParam("idNo") String idNO, @RequestParam("nama") String nama, @RequestParam("NIK") String NIK, @RequestParam("email") String email) {


        ResponseEntity<String> stringResponseEntity = customerService.customerKomplit(idNO, nama, NIK, email);
        return stringResponseEntity;

    }


    //validasi inputan notelpon
    public static Boolean isNumeric(String noTelepon) {
        if (noTelepon.matches("[0-9]+")) {
            return true;
        }
        return false;
    }

    @PutMapping("customer/update")
    public ResponseEntity<String> customerKomplit(@RequestParam("idNo") String idNO, @RequestParam("noTelepon")String noTelepon) {


        ResponseEntity<String> stringResponseEntity = customerService.customerKomplit(idNO, noTelepon);
        return stringResponseEntity;
    }

    @DeleteMapping("customer/delete")
    public ResponseEntity<String> customerKomplit(@RequestParam("idNo") String idNO) {

        ResponseEntity<String> stringResponseEntity = customerService.customerDelete(idNO);
        return stringResponseEntity;
    }

}
