package basyafinance.accountsaving.controller;


import basyafinance.accountsaving.service.UserLoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Slf4j
@RequestMapping("/api/registrasi")
public class UserLoginController {
    @Autowired
    private UserLoginService userLoginService;

    @PostMapping("/new/user")
    public ResponseEntity<String> saveUser(@RequestParam("username")String username, @RequestParam("password")String password, @RequestParam("name")String name){
        log.info("aa");
        ResponseEntity<String> stringResponseEntity = userLoginService.createLogin(username, password, name);
        log.info("");
        return stringResponseEntity;

    }
}
