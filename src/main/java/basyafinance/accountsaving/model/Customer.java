package basyafinance.accountsaving.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;
@Entity
@Setter
@Getter
@ToString
public class Customer {
    @Id
    @Type(type=("uuid-char"))
    private UUID idNo;
    private String noTelepon;
    private LocalDateTime TanggalPengajuan;
    private LocalDateTime TanggalUpdate;
    private String Status;
    private String Nama;
    private String NIK;
    private String email;
}
