package basyafinance.accountsaving.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @Type(type = ("uuid-char"))
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String name;
    private String username;
    private String password;
    private int failedAttempt;
    private boolean enabled;
    private boolean accountNonlocked;
    private Date lockTime;
    private Date failedTime;
    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Role> roles = new ArrayList<>();

    @PrePersist
    public void setCreatedAtBeforeInsert(){
        this.id = UUID.randomUUID();
    }

}
