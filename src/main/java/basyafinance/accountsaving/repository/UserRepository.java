package basyafinance.accountsaving.repository;

import basyafinance.accountsaving.model.user.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    User findByUsername(String username);

    @Query(value = "SELECT * FROM User WHERE username= :username", nativeQuery = true)
    User getUserByUsername(@Param("username") String username);

    @Query(value = "UPDATE User u SET u.failed_attempt = :failAttempts WHERE username = :username", nativeQuery = true)
    @Modifying
    void updateFailedAttempts(int failAttempts, String username);

}
