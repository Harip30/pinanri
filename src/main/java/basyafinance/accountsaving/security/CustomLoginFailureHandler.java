package basyafinance.accountsaving.security;

import basyafinance.accountsaving.model.user.CustomUserServices;
import basyafinance.accountsaving.model.user.User;
import basyafinance.accountsaving.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
@Component
@Slf4j
public class CustomLoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    @Autowired
    private CustomUserServices customUserServices;
    @Autowired
    private UserRepository userRepository;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {
        String username = request.getParameter("username");
        customUserServices.resetFailedAttemptWhenTimeExpired(username);
        User user = userRepository.getUserByUsername(username);
        String loginFailed = "Username / Password doesn't match, please check your password and try again.";

        if (user != null) {
            if (user.isEnabled() && user.isAccountNonlocked()) {
                if (user.getFailedAttempt() < CustomUserServices.MAX_FAILED_ATTEMPTS - 1) {
                    customUserServices.increaseFailedAttempts(user);
                    Map<String, String> responseData = new HashMap<>();
                    responseData.put("responseCode", "99");
                    responseData.put("responseDesc", loginFailed);
                    response.setContentType(APPLICATION_JSON_VALUE);
                    new ObjectMapper().writeValue(response.getOutputStream(), responseData);
                } else {
                    customUserServices.lock(user);
                    log.info("Your account has been locked due to 3 failed attempts."
                            + " It will be unlocked after 5 minutes.");
                    Map<String, String> responseData = new HashMap<>();
                    responseData.put("responseCode", "99");
                    responseData.put("responseDesc", "Your account has been locked due to 3 failed attempts."
                            + " It will be unlocked after 5 minutes.");
                    response.setContentType(APPLICATION_JSON_VALUE);
                    new ObjectMapper().writeValue(response.getOutputStream(), responseData);
                }
            } else if (!user.isAccountNonlocked()) {
                if (customUserServices.unlockWhenTimeExpired(user)) {
                    Map<String, String> responseData1 = new HashMap<>();
                    responseData1.put("responseCode", "99");
                    responseData1.put("responseDesc", "Your account has been unlocked. Please try to login again.");
                    response.setContentType(APPLICATION_JSON_VALUE);
                    new ObjectMapper().writeValue(response.getOutputStream(), responseData1);
                } else {
                    log.info("Your account has been unlocked.");
                    Map<String, String> responseData = new HashMap<>();
                    responseData.put("responseCode", "99");
                    responseData.put("responseDesc", "Your account locked,please try again later.");
                    response.setContentType(APPLICATION_JSON_VALUE);
                    new ObjectMapper().writeValue(response.getOutputStream(), responseData);
                }
            }
        }
        super.setDefaultFailureUrl("/api/login?error");
    }
}
