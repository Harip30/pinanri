package basyafinance.accountsaving.service;

import basyafinance.accountsaving.model.Customer;
import basyafinance.accountsaving.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public ResponseEntity<String> customerSave (String noTelepon){
        log.info("masuk ke serivice customer save");
        Customer customer = new Customer();
        customer.setIdNo(UUID.randomUUID());
        customer.setNoTelepon(noTelepon);
        customer.setTanggalPengajuan(LocalDateTime.now());
        customer.setStatus("step1");
        customerRepository.save(customer);
        log.info("selesai menyimpan data step1");
        log.info("nomor telepon yang disave = "+noTelepon);
        return ResponseEntity.ok("idNo: " +customer.getIdNo()+"\n"+"Status: " +customer.getStatus()+"\n"
                +"code: " +"00");

    }

    public ResponseEntity<String> customerKomplit (String idNo,String nama, String NIK, String email){
        UUID id = UUID.fromString(idNo);
        Optional<Customer> customer = customerRepository.findById(id);
        Customer customer1 = customer.get();
        customer1.setNama(nama);
        customer1.setNIK(NIK);
        customer1.setEmail(email);
        customer1.setStatus("step2");
        customerRepository.save(customer1);

        return ResponseEntity.ok("idNo: " +customer1.getIdNo()+"\n"+"status: "
                +customer1.getStatus()+"\n" +"code: " +"00");
    }

    public ResponseEntity<String> customerKomplit (String idNo, String noTelepon){
        UUID id = UUID.fromString(idNo);
        Optional<Customer>customer = customerRepository.findById(id);
        Customer customer1 = customer.get();
        customer1.setNoTelepon(noTelepon);
        customer1.setTanggalUpdate(LocalDateTime.now());
        customerRepository.save(customer1);

        return ResponseEntity.ok("idNo: " +customer1.getIdNo()+"\n"+"status: " +"update sukses");
    }

    public ResponseEntity<String> customerDelete (String idNo){
        UUID id = UUID.fromString(idNo);
        Optional<Customer>customer = customerRepository.findById(id);
        Customer customer1 = customer.get();
        customerRepository.delete(customer1);

        return ResponseEntity.ok("idNo: " + idNo +"\n"+ "status : berhasil delete");
    }


}
