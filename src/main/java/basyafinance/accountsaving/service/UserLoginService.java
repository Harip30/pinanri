package basyafinance.accountsaving.service;

import basyafinance.accountsaving.model.user.User;
import basyafinance.accountsaving.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserLoginService {
    @Autowired
    private UserRepository userRepository;
    public ResponseEntity<String> createLogin(String username, String password, String name){
        log.info("cek username"+ username);
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setName(name);
        user.setAccountNonlocked(true);
        user.setEnabled(true);
        userRepository.save(user);
        log.info("user berhasil disave");
        return ResponseEntity.ok("sukses = "+ username);
    }
}
