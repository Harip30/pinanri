package basyafinance.accountsaving.service;

import basyafinance.accountsaving.model.user.Role;
import basyafinance.accountsaving.model.user.User;

import java.util.List;

public interface UserService {
    User saveUsers(User user);
    Role saveRoles(Role role);

    User getUsers(String username);

    void addRolesToUsers(String username, String roleName);

    List<User> getUsers();
}
