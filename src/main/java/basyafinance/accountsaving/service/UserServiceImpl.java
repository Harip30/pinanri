package basyafinance.accountsaving.service;

import basyafinance.accountsaving.model.user.Role;
import basyafinance.accountsaving.model.user.User;
import basyafinance.accountsaving.repository.RoleRepository;
import basyafinance.accountsaving.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService,UserDetailsService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found in the database");
        } else {
            log.info("User in database:{}", username);
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }

    @Override
    public User saveUsers(User user) {
        log.info("saving new user{} to the database", user.getName());
                user.setPassword((passwordEncoder.encode(user.getPassword())));
        return userRepository.save(user);
    }

    @Override
    public Role saveRoles(Role role) {
        log.info("saving new role{} to the database", role.getName());
        return roleRepository.save(role);
    }

    @Override
    public void addRolesToUsers(String username, String roleName) {
        log.info("Adding role{}to user{}", roleName, username);
        User users = userRepository.findByUsername(username);
        Role roles = roleRepository.findByName(roleName);
        users.getRoles().add(roles);
    }

    @Override
    public User getUsers(String username) {
        log.info("Facting user{}", username);
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> getUsers() {
        log.info("Facting all user{}");
        return userRepository.findAll();
    }
}
